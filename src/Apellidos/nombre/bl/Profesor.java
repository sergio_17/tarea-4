package Apellidos.nombre.bl;

import java.time.LocalDate;

public class Profesor extends Usuario {

    private String TipoContrato;
    private LocalDate fechaContrato;

    public Profesor() {
        super();
        TipoContrato = "";
        this.fechaContrato = LocalDate.now();
        setTipo("Profesor");
    }

    public Profesor(int id, String nombre, String apellido, String tipoContrato, LocalDate fechaContrato) {
        super(id, nombre, apellido);
        TipoContrato = tipoContrato;
        this.fechaContrato = fechaContrato;
        setTipo("Profesor");
    }

    public String getTipoContrato() {return TipoContrato;}

    public void setTipoContrato(String tipoContrato) {TipoContrato = tipoContrato;}

    public LocalDate getFechaContrato() {  return fechaContrato; }

    public void setFechaContrato(LocalDate fechaContrato) { this.fechaContrato = fechaContrato; }

    public  String toStringUsuarios(){
        return UsuarioRD() + "," + TipoContrato + "," + fechaContrato;
    }

    @Override
    public String toString() {
        return super.toString() + TipoContrato + ", fechaContrato=" + fechaContrato ;
    }
}
