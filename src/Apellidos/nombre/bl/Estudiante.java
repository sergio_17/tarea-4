package Apellidos.nombre.bl;

public class Estudiante extends Usuario {
    private int Carnet;
    private String Carrera;
    private int Creditos;

    public Estudiante() {
        super();
        Carnet = 101;
        Carrera = "";
        Creditos = 0;
        setTipo("Estudiante");
    }

    public Estudiante(int id, String nombre, String apellido, int carnet, String carrera, int creditos) {
        super(id, nombre, apellido);
        Carnet = carnet;
        Carrera = carrera;
        Creditos = creditos;
        setTipo("Estudiante");
    }

    public int getCarnet() {return Carnet;}

    public void setCarnet(int carnet) {Carnet = carnet; }

    public String getCarrera() {return Carrera;}

    public void setCarrera(String carrera) {Carrera = carrera;}

    public int getCreditos() {return Creditos;}

    public void setCreditos(int creditos) {Creditos = creditos;}

    public  String toStringUsuarios(){
        return UsuarioRD() + "," + Carnet + "," + Carrera + "," + Creditos;
    }

    @Override
    public String toString() {
        return super.toString() + "Carnet=" + Carnet +", Carrera='" + Carrera + ", Creditos=" + Creditos ;
    }
}
