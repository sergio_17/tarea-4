package Apellidos.nombre.bl;

public class Administrativo  extends Usuario{
    private String TipoNombramiento;
    private int HorasAsignadas;

    public Administrativo() {
        super();
        TipoNombramiento = "";
        HorasAsignadas = 0;
        setTipo("Administrativo");
    }

    public Administrativo(int id, String nombre, String apellido, String tipoNombramiento, int horasAsignadas) {
        super(id, nombre, apellido);
        TipoNombramiento = tipoNombramiento;
        HorasAsignadas = horasAsignadas;
        setTipo("Administrativo");
    }

    public  String toStringUsuarios(){
        return UsuarioRD() + "," + TipoNombramiento + "," + HorasAsignadas;
    }

    @Override
    public String toString() {
        return "TipoNombramiento='" + TipoNombramiento +", HorasAsignadas=" + HorasAsignadas ;
    }
}
