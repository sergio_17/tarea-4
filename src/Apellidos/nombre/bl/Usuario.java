package Apellidos.nombre.bl;

public abstract class Usuario {
    private int Id;
    private String Nombre;
    private String Apellido;
    private String Tipo;

    public Usuario() {
        Id = 1;
        Nombre = "";
        Apellido = "";
        Tipo = "Usuario";
    }

    public Usuario(int id, String nombre, String apellido) {
        this.Id = id;
        this.Nombre = nombre;
        this.Apellido = apellido;
        this.Tipo = "Usuario";
    }

    public int getId() { return Id;}

    public void setId(int id) {  Id = id; }

    public String getNombre() { return Nombre;}

    public void setNombre(String nombre) { Nombre = nombre;}

    public String getApellido() {  return Apellido;}

    public void setApellido(String apellido) {Apellido = apellido; }

    public String getTipo() {return Tipo;}

    public void setTipo(String tipo) {Tipo = tipo; }

    public abstract String toStringUsuarios();

    public String UsuarioRD(){
        return Tipo + "," +  Id + "," + Nombre + "," + Apellido ;
    }

    @Override
    public String toString() {
        return "Id=" + Id + ", Nombre='" + Nombre + ", Apellido='" + Apellido;
    }

    public boolean equals(Object obj){
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof Usuario)) return false;

        Usuario item = (Usuario) obj;
        if (this.getId() == item.getId()) return true;

        return false;
    }
}
