package Apellidos.nombre.bl;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;

/*
* Clase RegistroBl se encarga del manejo de los datos de los usuarios cuando se van a registrar dentro de la plataforma.
* */
public class RegistroBL {

    private final String RegistroUsuarios  = "RegistroUsuarios.txt";

    public RegistroBL() {
    }

    /*
    * Metodo para registrar los datos dentro del archivo.
    * */
    public String registrarUsuarios (Usuario usuario){
        try {
            FileWriter writer = new FileWriter(RegistroUsuarios, true);
            BufferedWriter buffer = new BufferedWriter(writer);
            buffer.write(usuario.toStringUsuarios());
            buffer.newLine();
            buffer.close();
            return "El usuario se agrego de manera correcta";
        }catch (IOException ex){
            return "Error al registrar el usuario";
        }

    }

    public ArrayList<Usuario> listarUsuarios(){
        ArrayList<Usuario> lista  = new ArrayList<Usuario>();
        try {
            FileReader reader = new FileReader(RegistroUsuarios);
            BufferedReader buffer = new BufferedReader(reader);
            String registro = "";
            while ((registro = buffer.readLine()) != null){
                String[] datos = registro.split(",");
                Usuario material = convertirMaterial(datos);

                lista.add(material);
            }

        }catch (IOException ex){
            return new ArrayList<Usuario>();
        }
        return  lista;
    }

    /*
    * Metodo para validar la que no se repitan datos dentro del archivo
    * */
    public boolean validarUsuario(Usuario usuario){
        ArrayList<Usuario> listaUsuarios = listarUsuarios();
        boolean existe = false;

        for (Usuario registroUsuario: listaUsuarios) {
           if (usuario.equals(registroUsuario)) existe =  true;
        }

        return existe;
    }

    /*
    * Metodo para convertir los datos dependiendo del tipo de dato almacenado : Administrativo, Estudiante o Profesor
    * */
    private Usuario convertirMaterial(String[] datos){
        Usuario usuario = null;
        switch (datos[0]){
            case "Administrativo": new Administrativo(Integer.parseInt(datos[1]), datos[2], datos[3], datos[4], Integer.parseInt(datos[5]));
                break;
            case "Estudiante": new Estudiante(Integer.parseInt(datos[1]), datos[2], datos[3], Integer.parseInt(datos[4]), datos[5], Integer.parseInt(datos[6]));
                break;
            case "Profesor": new Profesor(Integer.parseInt(datos[1]), datos[2], datos[3],  datos[4], LocalDate.parse( datos[5]));
                break;
        }
        return usuario;
    }
}
