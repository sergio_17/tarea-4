package Apellidos.nombre.tl;

import Apellidos.nombre.bl.*;

import java.time.LocalDate;
import java.util.ArrayList;

public class UsuariosControlador {

    private RegistroBL logicaRegistro = new RegistroBL();

    public String registrarEstudiante(int id, String nombre, String apellido, int carnet, String carrera, int creditos){
        Estudiante estudiante = new Estudiante( id,  nombre,  apellido,  carnet,  carrera,  creditos);
        if (logicaRegistro.validarUsuario(estudiante)){
            return logicaRegistro.registrarUsuarios(estudiante);
        }
        return "El Estudiante: "  + nombre + " " + apellido + ". Ya existe dentro del sistema!";
    }

    public String registrarProfesor(int id, String nombre, String apellido, String tipoContrato, LocalDate fechaContrato){
        Profesor profesor = new Profesor( id,  nombre,  apellido,  tipoContrato,  fechaContrato);
        if (logicaRegistro.validarUsuario(profesor)) {
            return logicaRegistro.registrarUsuarios(profesor);
        }
        return "El profesor: " + nombre + " " + apellido + ". Ya existe dentro del sistema!";
    }

    public String registrarAdministrativo(int id, String nombre, String apellido, String tipoNombramiento, int horasAsignadas){
        Administrativo administrativo = new Administrativo( id,  nombre,  apellido,  tipoNombramiento,  horasAsignadas);
        if (logicaRegistro.validarUsuario(administrativo)) {
            return logicaRegistro.registrarUsuarios(administrativo);
        }
        return "El administrativo: " + nombre + " " + apellido + ". Ya existe dentro del sistema!";
    }

    public ArrayList<String> listarUsuarios(){
        ArrayList<Usuario> listaUsuarios = logicaRegistro.listarUsuarios();
        ArrayList<String> lista = new ArrayList<>();

        for (Usuario usuario: listaUsuarios) {
            lista.add(usuario.toString());
        }
        return lista;
    }
}
